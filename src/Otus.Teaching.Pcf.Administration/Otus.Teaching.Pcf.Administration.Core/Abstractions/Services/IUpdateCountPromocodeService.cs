﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IUpdateCountPromocodeService
    {
        public Task<bool> Update(Guid id);
    }
}
