﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Dto
{
    public class UpdatePromocodeConsumer : IConsumer<UpdatePromocodesToEmployeeDto>
    {
        private readonly IUpdateCountPromocodeService _updateCountPromocodeService;

        public UpdatePromocodeConsumer(IUpdateCountPromocodeService updateCountPromocodeService)
        {
            _updateCountPromocodeService = updateCountPromocodeService;
        }
        public async Task Consume(ConsumeContext<UpdatePromocodesToEmployeeDto> context)
        {
            await _updateCountPromocodeService.Update(context.Message.Id);
        }
    }
}
