﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class UpdateCountPromocodeService : IUpdateCountPromocodeService
    {
        private readonly IRepository<Employee> _empoloyeeRepository;

        public UpdateCountPromocodeService(IRepository<Employee> empoloyeeRepository)
        {
            _empoloyeeRepository = empoloyeeRepository;
        }

        public async Task<bool> Update(Guid id)
        {
            var employee = await _empoloyeeRepository.GetByIdAsync(id);

            if (employee == null)
                return false;
            employee.AppliedPromocodesCount++;

            await _empoloyeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}
