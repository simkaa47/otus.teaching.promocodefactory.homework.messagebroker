﻿
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationRabbit : IAdministrationGateway
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public AdministrationRabbit(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }
        public async  Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {            
            await _publishEndpoint.Publish<UpdatePromocodesToEmployeeDto>(new UpdatePromocodesToEmployeeDto { Id=partnerManagerId});            
        }


        
    }
}
